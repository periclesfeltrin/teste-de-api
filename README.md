# Teste de API

### Serviço para Teste
O serviço testato foi o da [ViaCEP](https://viacep.com.br) para verificação de CEPs.

### Cenários Desenvolvidos

URL: https://viacep.com.br/ws/91060900/json/

Cenário: Consulta CEP valido
Dado que o usuário inseri um CEP válido
Quando o serviço é consultado
Então é retornado o CEP, logradouro, complemento, bairro, localidade, uf e ibge.

Cenário: Consulta CEP inexistente
Dado que o usuário inseri um CEP que não exista na base dos Correios
Quando o serviço é consultado
Então é retornada um atributo erro

Cenário: Consulta CEP com formato inválido
Dado que o usuário inseri um CEP com formato inválido
Quando o serviço é consultado
Então é retornado uma mensagem de erro

Extras:
Criar um cenário que verifique o retorno do serviço abaixo:
URL: https://viacep.com.br/ws/RS/Gravatai/Barroso/json/

Obs.: Foi adicionado um cenário para validação do schema de retorno da API e outro para validar se a API estava "UP".

### Tecnologia
Tecnologias usadas para desenvolvimento:
- [Java](https://www.oracle.com/technetwork/pt/java/index.html)
- [RestAssured](http://rest-assured.io)
- [TestNG](https://testng.org/doc/)
- [Maven](https://maven.apache.org)

### Execução

Para executar via maven:

| execução | comando |
|-----|---------|
| Teste de healthcheck | `mvn test -Dsuite=healthcheck` |
| Teste de contrato | `mvn test -Dsuite=contrato` |
| Teste funcional | `mvn test -Dsuite=funcional` |

Também pode-se executar via a pipeline de testes do GitLab CI.
