package com.periclesfeltrin.api.funcional;

import com.periclesfeltrin.api.BaseTest;
import com.periclesfeltrin.api.DadosCep;
import com.periclesfeltrin.api.DadosCepFactory;
import io.restassured.http.ContentType;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

public class ConsultasCeps extends BaseTest {

    /*
        Cenário: Consulta CEP valido
        Dado que o usuário inseri um CEP válido
        Quando o serviço é consultado
        Então é retornado o CEP, logradouro, complemento, bairro, localidade, uf e ibge.

     */
    @Test(groups = "funcional", description = "Realizar a consulta de um CEP válido.")
    public void validarCepValido(){
        DadosCep dadosCep = DadosCepFactory.CepValido();

        given()
            .pathParam("cep", dadosCep.getCep())
            .pathParam("retorno", "json")
        .when()
            .get("{cep}/{retorno}/")
        .then()
            .statusCode(200)
            .body("cep", is(dadosCep.getCepFormatado()))
            .body("logradouro", is(dadosCep.getLogradouro()))
            .body("complemento", is(dadosCep.getComplemento()))
            .body("bairro", is(dadosCep.getBairro()))
            .body("localidade", is(dadosCep.getLocalidade()))
            .body("uf", is(dadosCep.getUf()))
            .body("ibge", is(dadosCep.getIbge()));
    }

    /*
        Cenário: Consulta CEP inexistente
        Dado que o usuário inseri um CEP que não exista na base dos Correios
        Quando o serviço é consultado
        Então é retornada um atributo erro

     */
    @Test(groups = "funcional", description = "Realizar a consulta de um CEP inexistente.")
    public void validarCepInexistente(){
        given()
            .pathParam("cep", "00000000")
            .pathParam("retorno", "json")
        .when()
            .get("{cep}/{retorno}/")
        .then()
            .statusCode(200)
            .body("erro",  is(true));
    }

    /*
    Cenário: Consulta CEP com formato inválido
        Dado que o usuário inseri um CEP com formato inválido
        Quando o serviço é consultado
        Então é retornado uma mensagem de erro
     */
    @Test(groups = "funcional", description = "Realizar a consulta de um CEP com formato inválido.")
    public void validarCepFormatoInvalido(){
        DadosCep dadosCep = DadosCepFactory.CepInvalido();

        Response response =
                given()
                    .pathParam("cep", dadosCep.getCep())
                    .pathParam("retorno", "json")
                .when()
                    .get("{cep}/{retorno}/")
                .then()
                    .statusCode(400)
                    .contentType(ContentType.HTML).extract()
                    .response();
        XmlPath doc_html = new XmlPath(XmlPath.CompatibilityMode.HTML, response.getBody().asString());
        Assert.assertEquals(doc_html.getString("html.body.h3"), "Verifique a sua URL (Bad Request)");
    }


    @Test(groups = "funcional", description = "Realizar a consulta de um CEP pelo Logradouro.")
    public void validarBuscaCepPorLogradouro(){
        DadosCep retorno_1 = DadosCep.builder()
                .cep("94085-170").logradouro("Rua Ari Barroso")
                .complemento("").bairro("Morada do Vale I")
                .localidade("Gravataí").uf("RS")
                .ibge("4309209").gia("")
                .ddd("51").siafi("8683").build();

        DadosCep retorno_2 = DadosCep.builder()
                .cep("94175-000").logradouro("Rua Almirante Barroso")
                .complemento("").bairro("Recanto Corcunda").localidade("Gravataí")
                .uf("RS").ibge("4309209").gia("")
                .ddd("51").siafi("8683").build();

        DadosCep[] retornos_api =
            given()
                .pathParam("uf", "RS")
                .pathParam("cidade", "Gravatai")
                .pathParam("logradouro", "Barroso")
                .pathParam("retorno", "json")
            .when()
                .get("{uf}/{cidade}/{logradouro}/{retorno}/")
            .then()
                .statusCode(200)
                .extract()
                .body()
                .as(DadosCep[].class);

        Assert.assertEquals(retornos_api.length, 2);
        Assert.assertEquals(retornos_api[0], retorno_1);
        Assert.assertEquals(retornos_api[1], retorno_2);

    }
}