package com.periclesfeltrin.api.health;

import com.periclesfeltrin.api.BaseTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.when;

public class HealthCheckTest extends BaseTest {
    @Test(groups = "healthcheck", description = "Verificar se a API está UP.")
    public void HealthCheck(){
        when()
            .get("91060900/json/")
        .then()
            .statusCode(200);
    }
}
