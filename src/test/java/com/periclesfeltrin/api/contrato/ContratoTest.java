package com.periclesfeltrin.api.contrato;

import com.periclesfeltrin.api.BaseTest;
import com.periclesfeltrin.api.DadosCep;
import com.periclesfeltrin.api.DadosCepFactory;
import org.testng.annotations.Test;

import java.io.File;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class ContratoTest extends BaseTest {
    @Test(groups="contrato", description = "Validar schema do retorno da API ViaCEP")
    public void validarContratoCep(){
        DadosCep dadosCep = DadosCepFactory.CepValido();

        given()
            .pathParam("cep", dadosCep.getCep())
            .pathParam("retorno", "json")
        .when()
            .get("{cep}/{retorno}/")
        .then()
            .statusCode(200)
            .body(matchesJsonSchema(
                    new File("src/test/resources/json_schemas/contrato_viacep_schema.json")
                    )
                );
    }
}
