package com.periclesfeltrin.api;

import org.testng.annotations.BeforeClass;

import static io.restassured.RestAssured.*;

public abstract class BaseTest {

    @BeforeClass(alwaysRun = true)
    public void preCodicao(){
        baseURI = "https://viacep.com.br/ws";
        basePath = "/";
        enableLoggingOfRequestAndResponseIfValidationFails();
    }
}
