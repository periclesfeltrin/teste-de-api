package com.periclesfeltrin.api;

public class DadosCepFactory {
    public static DadosCep CepValido(){
        return  DadosCep.builder()
                .cep("91060900").logradouro("Avenida Assis Brasil 3940")
                .complemento("").bairro("São Sebastião").localidade("Porto Alegre")
                .uf("RS").ibge("4314902").siafi("8801")
                .build();
    }

    public static DadosCep CepInvalido() {
        return  DadosCep.builder()
                .cep("91060T00").build();
    }
}