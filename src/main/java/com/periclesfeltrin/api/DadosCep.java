package com.periclesfeltrin.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class DadosCep {
    private String cep;
    private String logradouro;
    private String complemento;
    private String bairro;
    private String localidade;
    private String uf;
    private String ibge;
    private String gia;
    private String ddd;
    private String siafi;

    public String getCepFormatado() {
        String primeira_parte = cep.substring(0,5);
        String segunda_parte = cep.substring(5);
        return primeira_parte.concat("-").concat(segunda_parte);
    }
}
